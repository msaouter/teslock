﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class LineGestor : MonoBehaviour
{
    public float with = 1;
    
    // Start is called before the first frame update
    [ContextMenu("SetLine")]
    void SetLine()
    {
        for (int i = 0; i < transform.childCount-1; ++i)
        {
            GameObject point = transform.GetChild(i).gameObject;
            Vector3 distance = transform.GetChild(i + 1).position - point.transform.position;
            BoxCollider2D trigger = point.GetComponent<BoxCollider2D>();
            
            trigger.size = new Vector2(with, (distance).magnitude+0.1f);
            trigger.offset = new Vector2(0,(distance).magnitude/2);
            
            float rot_z = Mathf.Atan2(distance.y, distance.x) * Mathf.Rad2Deg ;
            point.transform.rotation = Quaternion.Euler(0f, 0f, rot_z-90);
            
            trigger.isTrigger = true;
        }    
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
