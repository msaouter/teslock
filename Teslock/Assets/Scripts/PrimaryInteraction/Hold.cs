﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hold : MonoBehaviour
{
    public float Timer = 2;
    private float internalTimer = 0;
    public Transform basePosition;
    public GameObject ImageRevealed;

    public GameObject BaseImage;
    

    private GameObject currentSprite;
    // Start is called before the first frame update
    void Start()
    {
        currentSprite = Instantiate(BaseImage, basePosition);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            if (internalTimer >= Timer)
            {
                GameObject.Destroy(currentSprite);
                currentSprite = Instantiate(ImageRevealed, basePosition);
            }

            internalTimer += Time.deltaTime;
        }
        else
        {
            internalTimer = 0;
            GameObject.Destroy(currentSprite);
            currentSprite = Instantiate(BaseImage, basePosition);
        }
    }
    
}
