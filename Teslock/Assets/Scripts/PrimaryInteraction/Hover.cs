﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hover : MonoBehaviour
{
    public Transform basePosition;
    public GameObject RevealedImage;
    public GameObject BaseImage;

    private GameObject currentSprite;

    private bool mouseOver = false;
    
    // Start is called before the first frame update
    void Start()
    {
        currentSprite = Instantiate(BaseImage, basePosition);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnMouseEnter()
    {
        mouseOver = true;
        GameObject.Destroy(currentSprite);
        currentSprite = Instantiate(RevealedImage, basePosition);
    }

    private void OnMouseExit()
    {
        mouseOver = false;
        GameObject.Destroy(currentSprite);
        currentSprite = Instantiate(BaseImage, basePosition);
    }
}
