﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Handle : MonoBehaviour
{
    private Camera _camera;

    private bool active;

    public Handle otherHandle;
    public GameObject pivot;
    
    
    public float speed = 0.5f;

    public bool inversed;

    public float angleCumul;


    // Start is called before the first frame update
    void Start()
    {
        _camera = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            
            Vector3 mousePos = _camera.ScreenToWorldPoint(Input.mousePosition);
            Vector2 direction = mousePos - transform.position;
            float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;

            if (inversed)
            {
                angle = -angle;
            }

            RaycastHit2D hit = Physics2D.Raycast(mousePos, Vector2.zero);
            if (hit.collider != null && hit.collider == this.GetComponent<Collider2D>())
            {
                this.active = true;
                angleCumul = Mathf.Clamp(angleCumul + Mathf.Sign(angle) * Time.deltaTime * speed, -37f, 37f);
                otherHandle.angleCumul = angleCumul;
                pivot.transform.rotation = Quaternion.Euler(0, 0, angleCumul);
            }
        }
        else
        {
            active = false;
        }
    }

    public bool isActive()
    {
        return active;
    }

    
}
