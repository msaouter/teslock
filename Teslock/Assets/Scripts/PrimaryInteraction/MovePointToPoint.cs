﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePointToPoint : MonoBehaviour
{
    /*public bool canDragVertical = true;
    public bool canDragHorizontal = true;*/

    //public Transform finalPos;
    
    private bool isDragging = false;
    private Camera _camera;

    private void Start()
    {
        _camera = Camera.main;
    }

    public void OnMouseDown()
    {
        isDragging = true;
    }

    public void OnMouseUp()
    {
        isDragging = false;
    }

    void Update()
    {
        if (isDragging)
        {
            Vector2 mousePosition = _camera.ScreenToWorldPoint(Input.mousePosition) - transform.position;
            transform.Translate(mousePosition);
        }
    }
}
