﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Click : MonoBehaviour
{
    public Transform basePosition;
    public GameObject ImageFirstClick;

    public GameObject ImageSecondClick;

    private GameObject currentSprite;
    //private Sprite newSprite;

    private int State;
    // Start is called before the first frame update
    void Start()
    {
        
        currentSprite = Instantiate(ImageFirstClick, basePosition);
        Debug.Log("Sprite instantiate");

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            //Debug.Log("mouse pressed");
            switch (State)
            {
                case 0: GameObject.Destroy(currentSprite);
                    currentSprite = Instantiate(ImageSecondClick, basePosition);
                    State = 1;
                    break;
                
                case 1: GameObject.Destroy(currentSprite);
                    currentSprite = Instantiate(ImageFirstClick, basePosition);
                    State = 0;
                    break;
                
            }
            
        }
    }
    
}
