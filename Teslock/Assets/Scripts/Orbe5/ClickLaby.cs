﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickLaby : MonoBehaviour
{
    public Checkpoint startPoint;
    public Checkpoint finishPoint;
    public List<Checkpoint> checkpoints;
    private List<Checkpoint> pointsClicked;
    
    private bool started = false;
    private bool allPoints = false;
    private bool finished = false;
    
    private Checkpoint currentPoint;
    private Checkpoint previousPoint;
    private Camera _camera;
    
    // Start is called before the first frame update
    void Start()
    {
        _camera = Camera.main;
        previousPoint = null;
        pointsClicked = new List<Checkpoint>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Vector3 mousePos = _camera.ScreenToWorldPoint(Input.mousePosition);
            Vector2 mousePos2D = new Vector2(mousePos.x, mousePos.y);

            RaycastHit2D hit = Physics2D.Raycast(mousePos2D, Vector2.zero);
            if (hit.collider != null )
            {
                if (started && !allPoints)
                {
                    currentPoint = hit.collider.gameObject.GetComponent<Checkpoint>();
                    if (currentPoint.IsNeighbour(previousPoint))
                    {
                        if (!pointsClicked.Contains((currentPoint)))
                        {
                            currentPoint.transform.GetChild(0).gameObject.SetActive(true);
                            //Debug.Log("Point " + currentPoint.name + " is now clicked");
                            /* tracer ligne */
                            pointsClicked.Add(currentPoint);
                            previousPoint = currentPoint;
                        }
                        else
                        {
                            Debug.Log("Point already in list and not all points selected");
                        }
                        
                        if (pointsClicked.Count == checkpoints.Count)
                        {
                            Debug.Log("all points clicked");
                            allPoints = true;
                        }
                        
                    }
                }

                else
                {
                    if (hit.collider.GetComponent<Checkpoint>() == startPoint)
                    {
                        Debug.Log(gameObject.name + "Puzzle started");
                        started = true;
                        previousPoint = hit.collider.gameObject.GetComponent<Checkpoint>();
                    }
                }

                if (allPoints)
                {
                    currentPoint = hit.collider.gameObject.GetComponent<Checkpoint>();
                    
                    print(currentPoint);

                    if (checkpoints[4] == previousPoint && currentPoint.IsNeighbour(previousPoint) && currentPoint.finish)
                    {
                        Debug.Log("Puzzle finished");
                        finished = true;
                    }

                }
            }
        }

        if (Input.GetMouseButtonDown(1))
        {
            Reset();
        }
    }

    public bool isFinished()
    {
        return finished;
    }
    
    
    public void Reset()
    {
        Debug.Log("reset");
        for (int i = 0; i < pointsClicked.Count; i++)
        {
            pointsClicked[i].transform.GetChild(0).gameObject.SetActive(false);
        }
        pointsClicked.Clear();
        started = false;
        allPoints = false;
        finished = false;
    }

    
}
