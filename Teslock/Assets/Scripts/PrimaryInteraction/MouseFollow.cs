﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseFollow : MonoBehaviour
{
    private Vector3 mousePosition;

    public float moveSpeed = 0.3f;

    private Rigidbody2D Rigidbody2D;

    private Vector2 position = new Vector2(0f, 0f);
    private Camera _camera;

    
    // Start is called before the first frame update
    void Start()
    {
        _camera = Camera.main;
        Rigidbody2D = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        //if (Input.GetMouseButton(0))
        {
            mousePosition = Input.mousePosition;
            mousePosition = _camera.ScreenToWorldPoint(mousePosition);
            position = Vector2.Lerp(transform.position, mousePosition, moveSpeed);
        }
        
    }

    private void FixedUpdate()
    {
        Rigidbody2D.MovePosition(position);
    }
    
    
    
}
