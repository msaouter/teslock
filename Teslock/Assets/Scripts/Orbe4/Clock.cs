﻿using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class Clock : MonoBehaviour
{
    private Camera _camera;
    
    [SerializeField] private Handle handle;
    //[SerializeField] private Clock clockNeedle;

    public GameObject pivot;

    public float speed;
    public bool counterclockwise = false;

    private float angle;
    private float angleCumul;

    [SerializeField]private float solutionAngle;
    [SerializeField] private float allowedDifference;
    
    // Start is called before the first frame update
    void Start()
    {
        _camera = Camera.main;
    }

    public bool isRightHour(Quaternion pivotRota)
    {
        if ((angleCumul * speed >= solutionAngle + allowedDifference) &&
            (angleCumul * speed <= solutionAngle - allowedDifference))
        {
            return true;
        }
        
        Debug.Log("angle cumul * speed : " + (angleCumul * speed));
        Debug.LogWarning("solution angle+ : " + (solutionAngle + allowedDifference));
        Debug.LogWarning("solution angle- : " + (solutionAngle - allowedDifference));

        return false;
    }
    
    // Update is called once per frame
    void Update()
    {
        if (handle.isActive())
        {
            //Debug.Log("Handle active");
            Vector3 mousePos = _camera.ScreenToWorldPoint(Input.mousePosition);
            Vector2 direction = mousePos - transform.position;
            angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
            
            if (counterclockwise)
            {
                angle = -angle;
            }
            angleCumul = handle.angleCumul + Mathf.Sign(angle);
            pivot.transform.rotation = Quaternion.Euler(0,0, angleCumul * speed);
            
            if (isRightHour(pivot.transform.rotation))
            {
                Debug.Log("Puzzle finished");
            }
        }
    }

    
}
