﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Labyrinthe : MonoBehaviour
{
    public ClickLaby topLaby;
    public ClickLaby bottomLaby;

    public GameObject finalIllumination;


    private Color color;
    public bool Orbe5Finished()
    {
        color = finalIllumination.GetComponent<SpriteRenderer>().color;
        var Tfinish = topLaby.isFinished();
        var Bfinish = bottomLaby.isFinished();
        //Debug.Log("toplaby finished : " + topLaby.isFinished() + "bottomlaby finished : " + bottomLaby.isFinished());
        
        if (Tfinish && Bfinish)
        {
            color.a = 1;
            finalIllumination.GetComponent<SpriteRenderer>().color = color;
            return true;
        }
        else if (Tfinish || Bfinish)
        {
            finalIllumination.SetActive(true);
            color.a = 0.5f;

            finalIllumination.GetComponent<SpriteRenderer>().color = color;
            
        }

        return false;
    }

    public void restartPuzzle()
    {
        finalIllumination.SetActive(false);
        topLaby.Reset();
        bottomLaby.Reset();
    }

    
}
