﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private static GameManager gameManager;

    private bool inGame;
    private int currentOrbe = 5;

    public Labyrinthe laby;
    public Canvas ui;
    
    public static GameManager _instance;
    public static GameManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<GameManager>();
                if(_instance == null)
                {
                    GameObject gameobj = new GameObject("GameManager");
                    _instance = gameobj.AddComponent<GameManager>();
                }
            }
            return _instance;

        }

    }

    
    // Start is called before the first frame update
    void Awake()
    {
        inGame = true;
    }

    void gameFinished()
    {
        var UI = ui.transform.GetChild(0).gameObject;
        if (currentOrbe > 5)
        {
            inGame = false;
            UI.SetActive(true);

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                UI.SetActive(false);
                
                
                laby.restartPuzzle();
                inGame = true;
                currentOrbe = 5;
            }
            //Debug.Log("Congrats");
        }
    }
    
    // Update is called once per frame
    void Update()
    {
        //Orbe 5
        if (currentOrbe == 5)
        {
            if (laby.Orbe5Finished())
            {
                currentOrbe++;
            }
        }

        gameFinished();
    }
}
