﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleMovement : MonoBehaviour
{
    private Vector3 lastMousePos;
    private Vector3 lastDirection;
    public float globalAngle =0;
    public bool active;
    private float elapsed = 0;
    private Camera _camera;


    // Update is called once per frame
    private void Start()
    {
        _camera = Camera.main;
    }

    
    //to be able to recognize multiple circles, don't desactivate active until player have
    //stopped pressed mouse button
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            globalAngle = 0;
            active = true;
            lastMousePos = Input.mousePosition;
            lastDirection = Vector3.zero;
        }
        else if (Input.GetMouseButtonUp(0)) active = false;
        
        if (active)
        {
            elapsed -= Time.deltaTime;

            if (elapsed <= 0)
            {
                Vector3 pos = _camera.ScreenToWorldPoint(Input.mousePosition);
                pos.z = 0;
                
                transform.position = pos;
                
                Vector3 direction = Input.mousePosition - lastMousePos;
                lastMousePos = Input.mousePosition;
                
                //Debug.Log(Vector3.SignedAngle(lastDirection, direction, Vector3.forward));
                globalAngle += Vector3.SignedAngle(lastDirection, direction, Vector3.forward);
                lastDirection = direction;
                
                elapsed = 0.1f;

                if (globalAngle > 350 || globalAngle < -350)
                {
                    active = false;
                }
            }
            
        }
    }
}
