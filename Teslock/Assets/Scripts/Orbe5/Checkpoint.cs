﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour
{
    public List<GameObject> neighbours;
    public bool finish;

    public bool IsNeighbour(Checkpoint clickedPoint)
    {
        //Debug.Log(gameObject.name + " cc ");
        
        var nameObj = clickedPoint.GetComponent(name);
        
        foreach (var neighbour in neighbours)
        {
            if (nameObj == neighbour.GetComponent(name))
            {
                return true;
            }
        }

        return false;
    }
}
